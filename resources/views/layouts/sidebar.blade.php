<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{asset('AdminLTE-2.4.18')}}/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Alexander Pierce</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      

      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li><a href="/"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
        <li class=" treeview">
          <a href="#">
            <i class=" fa fa-book"></i> <span>Master Data</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class=""><a href="{{ route('obat-get-all') }}"><i class="fa fa-circle-o"></i> Obat</a></li>
            <li><a href="{{ route('signa-get-all') }}"><i class="fa fa-circle-o"></i> Signa</a></li>
          </ul>
        </li>

        <li class=" treeview">
          <a href="#">
            <i class=" fa fa-book"></i> <span>Resep</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class=""><a href="{{ route('racikan-create') }}"><i class="fa fa-circle-o"></i> Racikan</a></li>
            <li><a href="{{ route('nonracikan-create') }}"><i class="fa fa-circle-o"></i> Non-Racikan</a></li>
          </ul>
        </li>
      </ul>
        
        
        
        
        
    </section>
    <!-- /.sidebar -->
  </aside>