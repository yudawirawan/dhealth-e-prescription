<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RacikanRef extends Model
{
    protected $table = "racikan_ref";
    protected $fillable = [
        'racikan_m_id',
        'obatalkes_m_id',
        'qty_racikan_ref'
    ];
}
