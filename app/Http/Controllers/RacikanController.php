<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Racikan;
use App\NonRacikan;
use App\ObatAlkes;
use App\Signa;

class RacikanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $racikan = DB::table('racikan_m')
                        ->join('signa_m', 'racikan_m.signa_m_id', '=', 'signa_m.signa_id')
                        ->get();
        return view('resep.racikan.index', compact('racikan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $obatalkes = DB::table('obatalkes_m')->get();
        $signa = DB::table('signa_m')->get();
        return view('resep.racikan.create', compact(['obatalkes', 'signa']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $obatalkes = DB::table('obatalkes_m')->get();
        
        $signa = DB::table('signa_m')->get();  

        $data = new Racikan;
        $data->nama = $request['nama'];
        $data->signa_m_id = $request['signa_m_id'];

        $data->save();
        $data->id;
        $racikanref = DB::table('racikan_ref')->where('racikan_m_id', '=', $data->id)->get();  

        return view('resep.racikan.createobat', compact(['data', 'obatalkes', 'racikanref', 'signa']));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $racikan = DB::table('racikan_m')->get();
        $racikan_ref = DB::table('racikan_ref')
                        ->where('nonracikan_m_id', $id)
                        ->get();
        return view('resep.racikan.show', compact(['racikan', 'racikan_ref']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
