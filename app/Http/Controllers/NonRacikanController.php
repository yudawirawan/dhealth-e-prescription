<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NonRacikan;
use App\ObatAlkes;
use App\Signa;
use DB;

class NonRacikanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $non_racikan = DB::table('nonracikan')
                            ->join('obatalkes_m', 'nonracikan.obatalkes_m_id', '=', 'obatalkes_m.obatalkes_id')
                            ->join('signa_m', 'nonracikan.signa_m_id', '=', 'signa_m.signa_id')
                            ->get();

        return view('resep.nonracikan.index', ['non_racikan' => $non_racikan]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $obatalkes = DB::table('obatalkes_m')->get();
        $signa = DB::table('signa_m')->get();
        return view('resep.nonracikan.create', compact(['obatalkes', 'signa']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'obatalkes_m_id' => 'required',
            'signa_m_id' => 'required',
            'qty_nonracikan' => 'required',
        ]);
  
        NonRacikan::create($request->all());

        return redirect()->route('nonracikan-create')
                         ->with(['success' => 'Data Resep Berhasil Ditambahkan']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $data = array(
        //     'non_racikan' => NonRacikan::findOrFail($id),
        //     'obatalkes' => ObatAlkes::all(),
        //     'signa' => Signa::all(),
        // );

        $data = NonRacikan::findOrFail($id);
        $signa = DB::table('signa_m')->get();
        $obatalkes = DB::table('obatalkes_m')->get();

        return view('resep.nonracikan.edit',compact(['data', 'signa', 'obatalkes']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'signa_m_id' => 'required',
        ]);
        
        $non_racikan = NonRacikan::findOrFail($id);
        $non_racikan->update([
            'signa_m_id' => $request->signa_m_id,
        ]);
        return redirect()->route('nonracikan-get-all')
                        ->with('success', 'Data Resep Berhasil Diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = NonRacikan::findOrFail($id);
        $data->delete();
        return redirect()->route('nonracikan-get-all')
                        ->with('success', 'Data Resep Berhasil Dihapus');
    }
}
