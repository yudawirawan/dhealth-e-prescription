<?php

namespace App\Http\Controllers;

use App\RacikanRef;
use Illuminate\Http\Request;
use DB;
use App\Racikan;
use App\ObatAlkes;
use App\Signa;

class RacikanRefController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $data = Racikan::findOrFail($id);

        $obatalkes = DB::table('obatalkes_m')->get();
        $signa = DB::table('signa_m')->get();
        return view('resep.racikan.createobat', compact(['obatalkes', 'signa', 'data']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $request->validate([
        //     'racikan_m_id' => 'required',
        //     'obatalkes_m_id' => 'required',
        //     'qty_racikan_ref' => 'required',
        // ]);
  
        // RacikanRef::create($request->all());

        $data = new RacikanRef;
        $data->racikan_m_id = $request['racikan_m_id'];
        $data->obatalkes_m_id = $request['obatalkes_m_id'];
        $data->qty_racikan_ref = $request['qty_racikan_ref'];
 
        $data->save();
        $data->racikan_m_id;

        $obatalkes = DB::table('obatalkes_m')->get();
        $signa = DB::table('signa_m')->get(); 
        $racikanref = DB::table('racikan_ref')->where('racikan_m_id', '=', $data->racikan_m_id)->get();  

        return view('resep.racikan.createobat', compact(['data', 'obatalkes', 'racikanref', 'signa']));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RacikanRef  $RacikanRef
     * @return \Illuminate\Http\Response
     */
    public function show(RacikanRef $RacikanRef)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RacikanRef  $RacikanRef
     * @return \Illuminate\Http\Response
     */
    public function edit(RacikanRef $RacikanRef)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RacikanRef  $RacikanRef
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RacikanRef $RacikanRef)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RacikanRef  $RacikanRef
     * @return \Illuminate\Http\Response
     */
    public function destroy(RacikanRef $RacikanRef)
    {
        //
    }
}
