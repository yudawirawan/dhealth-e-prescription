<?php

namespace App\Http\Controllers;

use App\ObatAlkes;
use Illuminate\Http\Request;
use DB;

class ObatAlkesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $obat_alkes = DB::table('obatalkes_m')->get();

        return view('master.obat.index', ['obat_alkes' => $obat_alkes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ObatAlkes  $obatAlkes
     * @return \Illuminate\Http\Response
     */
    public function show(ObatAlkes $obatAlkes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ObatAlkes  $obatAlkes
     * @return \Illuminate\Http\Response
     */
    public function edit(ObatAlkes $obatAlkes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ObatAlkes  $obatAlkes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ObatAlkes $obatAlkes)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ObatAlkes  $obatAlkes
     * @return \Illuminate\Http\Response
     */
    public function destroy(ObatAlkes $obatAlkes)
    {
        //
    }
}
