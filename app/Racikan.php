<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Racikan extends Model
{
    protected $table = "racikan_m";
    protected $fillable = [
        'signa_m_id',
        'nama',
    ];
}
