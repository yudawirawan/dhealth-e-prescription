<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NonRacikan extends Model
{
    protected $table = "nonracikan";
    protected $fillable = [
        'obatalkes_m_id',
        'signa_m_id',
        'qty_nonracikan',
    ];
}
