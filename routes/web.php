<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', function () {
    return view('layouts.master');
});

// API master data
Route::group(['prefix' => 'master'], function(){

    Route::group(['prefix' => 'obat'], function(){
        Route::get('/', 'ObatAlkesController@index')->name('obat-get-all');
    });

    Route::group(['prefix' => 'signa'], function(){
        Route::get('/', 'SignaController@index')->name('signa-get-all');
    });

});

//API Resep
Route::group(['prefix' => 'resep'], function(){

    Route::group(['prefix' => 'racikan'], function(){
        Route::get('/', 'RacikanController@index')->name('racikan-get-all');
        Route::get('/add', 'RacikanController@create')->name('racikan-create');
        Route::get('edit/{id}', 'RacikanController@edit')->name('racikan-edit');
        Route::post('/', 'RacikanController@store')->name('racikan-insert');
        Route::post('update/{id}', 'RacikanController@update')->name('racikan-update');
        Route::get('/{id}', 'RacikanController@destroy')->name('racikan-delete');
        Route::get('/{id}', 'RacikanRefController@create')->name('racikanref-create');
        Route::post('/ref', 'RacikanRefController@store')->name('racikanref-insert');
    });

    Route::group(['prefix' => 'non-racikan'], function(){
        Route::get('/', 'NonRacikanController@index')->name('nonracikan-get-all');
        Route::get('/add', 'NonRacikanController@create')->name('nonracikan-create');
        Route::get('edit/{id}', 'NonRacikanController@edit')->name('nonracikan-edit');
        Route::post('/', 'NonRacikanController@store')->name('nonracikan-insert');
        Route::post('update/{id}', 'NonRacikanController@update')->name('nonracikan-update');
        Route::get('/{id}', 'NonRacikanController@destroy')->name('nonracikan-delete');
    });

});
